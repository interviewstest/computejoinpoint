package com.codingame.interviews;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class ComputeJointPointTest {

	@Test(expected = ComputeJointPointNotFoundException.class)
	public void siUnParamOuLesDeuxSontNullsAlorsThrowException() throws ComputeJointPointNotFoundException {
		int s1 = 0, s2 = 1;
		ComputeJointPoint.computeJointPoint(s1, s2);
	}

	@Deprecated
	@Test
	public void siUnParamExisteDansSetAlorsTrue() throws ComputeJointPointNotFoundException {
		int s1 = 57, s2 = 78;
		assertTrue(ComputeJointPoint.computeJointPoint(s1, s2) == 111);
	}
}
