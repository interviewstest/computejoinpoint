package com.codingame.interviews;

public class ComputeJointPointNotFoundException extends Exception {

	String message;
	
	public ComputeJointPointNotFoundException(String message) {
		super(message);
	}
}
