package com.codingame.interviews;

public class ComputeJointPointNotFoundException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	String message;
	
	public ComputeJointPointNotFoundException(String message) {
		super(message);
	}
}
