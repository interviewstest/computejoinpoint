package com.codingame.interviews;

import java.util.HashSet;
import java.util.Set;
/**
 * 
 * 	On suppose que:
 * 				-	on a que des params > 0
 *  			-	on auras tjrs un point de jointure 
 *	On garde l'historique des nouveaux params dans une set et on verifie à chaque fois si le nouveau param etait déjà stocké. 
 */
public class ComputeJointPoint {

	public static int computeJointPoint(int s1, int s2) throws ComputeJointPointNotFoundException {
		if (s1 == 0 || s2 == 0) {
			throw new ComputeJointPointNotFoundException("Compute join point does not existe.");
		}
		Set<Integer> s1Set = new HashSet<>();
		Set<Integer> s2Set = new HashSet<>();
		while (s1 != s2) {
			s1Set.add(s1);
			s2Set.add(s2);
			if (s1Set.contains(s2)) {
				return s2;
			}
			if (s2Set.contains(s1)) {
				return s1;
			}
			s1 = nextVal(s1);
			s2 = nextVal(s2);
		}
		return s1;
	}

	static int nextVal(int s) {
		int sum = 0, tmp = s;// pour stocker le varibale qui va changer

		while (tmp > 0) {
			int modulo = tmp % 10; // 5
			sum += modulo;
			tmp = tmp / 10;
		}
		return sum + s;
	}
}
